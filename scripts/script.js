/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');
const TouchGestures = require('TouchGestures');
const Random = require('Random');
const Time = require('Time');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

var theAnswer = 0;
var gameActive = false;
var gameTime = 0;
var answerTime = 10;
const questionTime = 10;
var questionCount = 0;
var gameScore = 0;

const equation = Scene.root.find('2dText0');
const startButton = Scene.root.find("rectangle0");
const startText = Scene.root.find("startText");
const timeText = Scene.root.find("timeText");
const scoreText = Scene.root.find("scoreText");

let gameTimer = Time.setInterval(()=>{
  if (!gameActive) { return }
  gameTime += 1;
  answerTime -= 1;
  // Diagnostics.log(gameTime);
  timeText.text = answerTime.toString();
//  timeText.text = (11 - (gameTime % (questionTime))).toString();
  if (answerTime == 0) {
//  if (gameTime % questionTime == 0) {
    genQuestion();
    timeText.text = "10";
    answerTime = 10;
  }
},1000);

var answers = [
  Scene.root.find("Ans0"),
  Scene.root.find("Ans1"),
  Scene.root.find("Ans2"),
  Scene.root.find("Ans3")
];
var answerVals = [ 0,0,0,0 ];

var planes = [
  Scene.root.find("plane0"),
  Scene.root.find("plane1"),
  Scene.root.find("plane2"),
  Scene.root.find("plane3")
];

answers.forEach (a => {
  a.transform.scaleX = 0.01125;
  a.transform.scaleY = 0.01125;
  // a.worldTransform.positionX = 10;
  a.hidden = true;

//  TouchGestures.onTap(a).subscribe(answerTapped);
/*
  TouchGestures.onTap(a).subscribe((gesture) => {
    a.hidden = true;
    let ans = parseInt(a.text);
    if (ans == theAnswer) {
      Diagnostics.log("CORRECT");
    } else {
      Diagnostics.log("WRONG");
    }
  });
  */
});

planes.forEach (p => {
  TouchGestures.onTap(p).subscribe((gesture) => {
    let idx = planes.indexOf(p);
    if (idx < 0) { return }
    let a = answers[idx];
    a.hidden = true;
    // let ans = parseInt(a.text);
    let ans = answerVals[idx];
    // Diagnostics.log(idx);
    // Diagnostics.log(answerVals[idx]);
    if (ans == theAnswer) {
      Diagnostics.log("CORRECT");
      gameScore += answerTime;
      scoreText.text = gameScore.toString();

      genQuestion();
      timeText.text = "10";
      answerTime = 10;
  
    } else {
      Diagnostics.log("WRONG");
      gameScore -= answerTime;
      scoreText.text = gameScore.toString();
    }
  });
});

function endGame() {
  gameActive = false;
  equation.text = "?  ?  ?  ?";
  answers.forEach( a => {
    a.hidden = true;
  });
  startText.hidden = false;
  startButton.hidden = false;

}

// function answerTapped(gesture) {
//   let a = gesture.
// }

function genQuestion() {
  questionCount += 1;
  if (questionCount == 10) {
    endGame();
    return;
  }
  updateEquation();
  genAnswers();
}

TouchGestures.onTap(startButton).subscribe(function (gesture) {
  gameActive = true;
  gameTime = 0;
  questionCount = 0;
  gameScore = 0;
  scoreText.text = "0";
  genQuestion();
  startButton.hidden = true;
  startText.hidden = true;
});

function genAnswers() {
  var answerSet = new Set([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]);
  // for (var i=1; i<=20; i++) { answerSet.add(i); }
  answerSet.delete(theAnswer);

  answers.forEach ( (a,i) => {
    // a.text = "";
    const aa = [...answerSet];
    const idx = Math.floor(Random.random() * aa.length);
//    Diagnostics.log(aa);
//    Diagnostics.log(idx);
    const val = aa[idx];
//    Diagnostics.log(val);
    answerSet.delete(val);
    a.text = val.toString();
    answerVals[i] = val;
    // a.worldTransform.positionX = 10;
    // a.transform.positionY = -10;
    a.hidden = false;
  });
  const aPos = Math.floor(Random.random() * answers.length);
  answers[aPos].text = theAnswer.toString();
  answerVals[aPos] = theAnswer;

  /*
  answers.forEach ( a => {
    if (a.text.length == 0) {
    }
    a.hidden = false;
  });
  */
}


function updateEquation() {

  const a = Math.round(Random.random() * 20) + 2;
  const b = Math.round(Random.random() * 10) + 2;
//  const c = Math.round(Random.random() * 5) + 2;

  const d = a * b;
  var opts = [];
  for (var i = 2; i < d; i++) {
    if (d % i == 0) { opts.push(i) }
  }
  const idx = Math.floor(Random.random() * opts.length);
  const c = opts[idx];

//  Diagnostics.log(opts);

//  Diagnostics.log(a);
  equation.text = "("+a+" x "+b+") / "+c;

  theAnswer = d / c;

  //equation.text = "(8 x 3) / 4";
  Diagnostics.log('Updated equation');  
}


// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');
